import {browser, by, element} from 'protractor';

export class ProtokollVy {
  async navigeraTill(): Promise<unknown> {
    return browser.get(browser.baseUrl);
  }

  async registreraTärningssummaFörEttor(tärningssumma: number, spelareId: string) {
    this.registreraTärningssumma(tärningssumma, spelareId, "ettor");
  }

  async läsTärningssummaFörEttor(spelareId: string): Promise<string> {
    return this.läsTärningssumma(spelareId, "ettor");
  }

  async läsSumma(spelareId: string): Promise<string> {
    return element(by.css(`#${spelareId} .summa .poäng`)).getText();
  }

  async läsTotalsumma(spelareId: string): Promise<string> {
    return element(by.css(`#${spelareId} .totalt .poäng`)).getText();
  }

  async registreraTärningssummaFörTvåor(tärningssumma: number, spelareId: string) {
    this.registreraTärningssumma(tärningssumma, spelareId, "tvåor");
  }

  async läsTärningssummaFörTvåor(spelareId: string) {
    return this.läsTärningssumma(spelareId, "tvåor");
  }

  async registreraTärningssummaFörTreor(tärningssumma: number, spelareId: string) {
    this.registreraTärningssumma(tärningssumma, spelareId, "treor");
  }

  async läsTärningssummaFörTreor(spelareId: string) {
    return this.läsTärningssumma(spelareId, "treor");
  }

  async registreraTärningssummaFörFyror(tärningssumma: number, spelareId: string) {
    this.registreraTärningssumma(tärningssumma, spelareId, "fyror");
  }

  async läsTärningssummaFörFyror(spelareId: string) {
    return this.läsTärningssumma(spelareId, "fyror");
  }

  async registreraTärningssummaFörFemmor(tärningssumma: number, spelareId: string) {
    this.registreraTärningssumma(tärningssumma, spelareId, "femmor");
  }

  async läsTärningssummaFörFemmor(spelareId: string) {
    return this.läsTärningssumma(spelareId, "femmor");
  }

  async registreraTärningssummaFörSexor(tärningssumma: number, spelareId: string) {
    this.registreraTärningssumma(tärningssumma, spelareId, "sexor");
  }

  async läsTärningssummaFörSexor(spelareId: string) {
    return this.läsTärningssumma(spelareId, "sexor");
  }

  async registreraTärningssummaFörEttPar(tärningssumma: number, spelareId: string) {
    this.registreraTärningssumma(tärningssumma, spelareId, "ettPar");
  }

  async läsTärningssummaFörEttPar(spelareId: string) {
    return this.läsTärningssumma(spelareId, "ettPar");
  }

  async registreraTärningssummaFörTvåPar(tärningssumma: number, spelareId: string) {
    this.registreraTärningssumma(tärningssumma, spelareId, "tvaPar");
  }

  async läsTärningssummaFörTvåPar(spelareId: string) {
    return this.läsTärningssumma(spelareId, "tvaPar");
  }

  async registreraTärningssummaFörTretal(tärningssumma: number, spelareId: string) {
    this.registreraTärningssumma(tärningssumma, spelareId, "tretal");
  }

  async läsTärningssummaFörTretal(spelareId: string) {
    return this.läsTärningssumma(spelareId, "tretal");
  }

  async registreraTärningssummaFörFyrtal(tärningssumma: number, spelareId: string) {
    this.registreraTärningssumma(tärningssumma, spelareId, "fyrtal");
  }

  async läsTärningssummaFörFyrtal(spelareId: string) {
    return this.läsTärningssumma(spelareId, "fyrtal");
  }

  async registreraTärningssummaFörLitenStege(tärningssumma: number, spelareId: string) {
    this.registreraTärningssumma(tärningssumma, spelareId, "litenStege");
  }

  async läsTärningssummaFörLitenStege(spelareId: string) {
    return this.läsTärningssumma(spelareId, "litenStege");
  }

  async registreraTärningssummaFörStorStege(tärningssumma: number, spelareId: string) {
    this.registreraTärningssumma(tärningssumma, spelareId, "storStege");
  }

  async läsTärningssummaFörStorStege(spelareId: string) {
    return this.läsTärningssumma(spelareId, "storStege");
  }

  async registreraTärningssummaFörKåk(tärningssumma: number, spelareId: string) {
    this.registreraTärningssumma(tärningssumma, spelareId, "kak");
  }

  async läsTärningssummaFörKåk(spelareId: string) {
    return this.läsTärningssumma(spelareId, "kak");
  }

  async registreraTärningssummaFörChans(tärningssumma: number, spelareId: string) {
    this.registreraTärningssumma(tärningssumma, spelareId, "chans");
  }

  async läsTärningssummaFörChans(spelareId: string) {
    return this.läsTärningssumma(spelareId, "chans");
  }

  async registreraTärningssummaFörYatzy(tärningssumma: number, spelareId: string) {
    this.registreraTärningssumma(tärningssumma, spelareId, "yatzy");
  }

  async läsTärningssummaFörYatzy(spelareId: string) {
    return this.läsTärningssumma(spelareId, "yatzy");
  }

  private registreraTärningssumma(tärningssumma: number, spelareId: string, villkor: string) {
    return element(by.css(`#${spelareId} [name="${villkor}"]`)).sendKeys(tärningssumma);
  }

  private läsTärningssumma(spelareId: string, villkor: string) {
    return element(by.css(`#${spelareId} [name="${villkor}"]`)).getAttribute("value");
  }
}
