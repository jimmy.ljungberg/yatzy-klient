import {browser, logging} from 'protractor';
import {ProtokollVy} from "./protokoll.po";

["spelare-1", "spelare-2"].forEach((spelareId) => {
  describe(`Registrera tärningssummor för ${spelareId}`, () => {
    let vy: ProtokollVy;

    beforeEach(() => {
      vy = new ProtokollVy();
    });

    it('borde registrera 3 som tärningssumma för ettor', async () => {
      await vy.navigeraTill();
      await vy.registreraTärningssummaFörEttor(3, spelareId);
      expect(await vy.läsTärningssummaFörEttor(spelareId)).toEqual("3");
      expect(await vy.läsSumma(spelareId)).toEqual("3");
      expect(await vy.läsTotalsumma(spelareId)).toEqual("3");
    });

    it('borde registrera 4 som tärningssumma för tvåor', async () => {
      await vy.navigeraTill();
      await vy.registreraTärningssummaFörTvåor(4, spelareId);
      expect(await vy.läsTärningssummaFörTvåor(spelareId)).toEqual("4");
      expect(await vy.läsSumma(spelareId)).toEqual("4");
      expect(await vy.läsTotalsumma(spelareId)).toEqual("4");
    });

    it('borde registrera 3 som tärningssumma för treor', async () => {
      await vy.navigeraTill();
      await vy.registreraTärningssummaFörTreor(3, spelareId);
      expect(await vy.läsTärningssummaFörTreor(spelareId)).toEqual("3");
      expect(await vy.läsSumma(spelareId)).toEqual("3");
      expect(await vy.läsTotalsumma(spelareId)).toEqual("3");
    });

    it('borde registrera 12 som tärningssumma för fyror', async () => {
      await vy.navigeraTill();
      await vy.registreraTärningssummaFörFyror(12, spelareId);
      expect(await vy.läsTärningssummaFörFyror(spelareId)).toEqual("12");
      expect(await vy.läsSumma(spelareId)).toEqual("12");
      expect(await vy.läsTotalsumma(spelareId)).toEqual("12");
    });

    it('borde registrera 20 som tärningssumma för femmor', async () => {
      await vy.navigeraTill();
      await vy.registreraTärningssummaFörFemmor(20, spelareId);
      expect(await vy.läsTärningssummaFörFemmor(spelareId)).toEqual("20");
      expect(await vy.läsSumma(spelareId)).toEqual("20");
      expect(await vy.läsTotalsumma(spelareId)).toEqual("20");
    });

    it('borde registrera 12 som tärningssumma för sexor', async () => {
      await vy.navigeraTill();
      await vy.registreraTärningssummaFörSexor(12, spelareId);
      expect(await vy.läsTärningssummaFörSexor(spelareId)).toEqual("12");
      expect(await vy.läsSumma(spelareId)).toEqual("12");
      expect(await vy.läsTotalsumma(spelareId)).toEqual("12");
    });

    it('borde registrera 8 som tärningssumma för "ett par"', async () => {
      await vy.navigeraTill();
      await vy.registreraTärningssummaFörEttPar(8, spelareId);
      expect(await vy.läsTärningssummaFörEttPar(spelareId)).toEqual("8");
      expect(await vy.läsTotalsumma(spelareId)).toEqual("8");
    });

    it('borde registrera 14 som tärningssumma för "två par"', async () => {
      await vy.navigeraTill();
      await vy.registreraTärningssummaFörTvåPar(14, spelareId);
      expect(await vy.läsTärningssummaFörTvåPar(spelareId)).toEqual("14");
      expect(await vy.läsTotalsumma(spelareId)).toEqual("14");
    });

    it('borde registrera 15 som tärningssumma för tretal', async () => {
      await vy.navigeraTill();
      await vy.registreraTärningssummaFörTretal(15, spelareId);
      expect(await vy.läsTärningssummaFörTretal(spelareId)).toEqual("15");
      expect(await vy.läsTotalsumma(spelareId)).toEqual("15");
    });

    it('borde registrera 4 som tärningssumma för fyrtal', async () => {
      await vy.navigeraTill();
      await vy.registreraTärningssummaFörFyrtal(4, spelareId);
      expect(await vy.läsTärningssummaFörFyrtal(spelareId)).toEqual("4");
      expect(await vy.läsTotalsumma(spelareId)).toEqual("4");
    });

    it('borde registrera 15 som tärningssumma för "liten stege"', async () => {
      await vy.navigeraTill();
      await vy.registreraTärningssummaFörLitenStege(15, spelareId);
      expect(await vy.läsTärningssummaFörLitenStege(spelareId)).toEqual("15");
      expect(await vy.läsTotalsumma(spelareId)).toEqual("15");
    });

    it('borde registrera 20 som tärningssumma för "stor stege"', async () => {
      await vy.navigeraTill();
      await vy.registreraTärningssummaFörStorStege(20, spelareId);
      expect(await vy.läsTärningssummaFörStorStege(spelareId)).toEqual("20");
      expect(await vy.läsTotalsumma(spelareId)).toEqual("20");
    });

    it('borde registrera 25 som tärningssumma för kåk', async () => {
      await vy.navigeraTill();
      await vy.registreraTärningssummaFörKåk(25, spelareId);
      expect(await vy.läsTärningssummaFörKåk(spelareId)).toEqual("25");
      expect(await vy.läsTotalsumma(spelareId)).toEqual("25");
    });

    it('borde registrera 18 som tärningssumma för chans', async () => {
      await vy.navigeraTill();
      await vy.registreraTärningssummaFörChans(18, spelareId);
      expect(await vy.läsTärningssummaFörChans(spelareId)).toEqual("18");
      expect(await vy.läsTotalsumma(spelareId)).toEqual("18");
    });

    it('borde registrera 50 som tärningssumma för yatzy', async () => {
      await vy.navigeraTill();
      await vy.registreraTärningssummaFörYatzy(50, spelareId);
      expect(await vy.läsTärningssummaFörYatzy(spelareId)).toEqual("50");
      expect(await vy.läsTotalsumma(spelareId)).toEqual("50");
    });

    afterEach(async () => {
      // Assert that there are no errors emitted from the browser
      const logs = await browser.manage().logs().get(logging.Type.BROWSER);
      expect(logs).not.toContain(jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry));
    });
  });
});
