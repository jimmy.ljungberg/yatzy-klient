import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Spelare} from "../spelare";

@Component({
  selector: 'app-poang',
  templateUrl: './poang.component.html',
  styleUrls: ['./poang.component.scss']
})
export class PoangComponent implements OnInit {
  @Input()
  spelare: Spelare = new Spelare("Dummy", 1);
  @Input()
  tarningar: number[] = [];
  @Output()
  avslutatSpeldrag: EventEmitter<number> = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit(): void {
  }

  summeraEttor(tärningar: number[]): number {
    return tärningar
      .filter((tärning) => tärning === 1)
      .reduce((summa, tärning) => summa + tärning, 0);
  }

  sparaEttor(tärningar: number[]) {
    this.spelare.ettor = this.summeraEttor(tärningar);
    this.avslutatSpeldrag.emit(this.spelare.id);
  }

  farSparaEttor(tärningar: number[]): boolean {
    return tärningar.length > 0 && !this.spelare.ettor;
  }

  summeraTvaor(tärningar: number[]): number {
    return tärningar
      .filter((tärning) => tärning === 2)
      .reduce((summa, tärning) => summa + tärning, 0);
  }

  farSparaTvaor(tärningar: number[]): boolean {
    return tärningar.length > 0 && !this.spelare.tvaor;
  }

  sparaTvaor(tärningar: number[]) {
    this.spelare.tvaor = this.summeraTvaor(tärningar);
    this.avslutatSpeldrag.emit(this.spelare.id);
  }

  summeraEttPar(tärningar: number[]): number {
    return [1, 2, 3, 4, 5, 6]
      .map((sida) => this.summeraPar(tärningar, sida))
      .reduce((maxSumma, summa) => summa > maxSumma ? summa : maxSumma, 0);
  }

  private summeraPar(tärningar: number[], tärningssida: number) {
    return tärningar
      .filter((tärning) => tärning === tärningssida).length > 1 ? tärningssida * 2 : 0;
  }
}
