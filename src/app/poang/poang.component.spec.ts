import {ComponentFixture, TestBed} from '@angular/core/testing';

import {PoangComponent} from './poang.component';
import {FormsModule} from "@angular/forms";

describe('PoangComponent', () => {
  let component: PoangComponent;
  let fixture: ComponentFixture<PoangComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [PoangComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PoangComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe("summera ettor", () => {
    it("borde returnera 0 när endast nollor finns", () => {
      expect(component.summeraEttor([0, 0, 0, 0, 0])).toEqual(0);
    });

    it("borde returnera 5 när fem ettor finns", () => {
      expect(component.summeraEttor([1, 1, 1, 1, 1])).toEqual(5);
    });

    it("borde returnera 3 när tre ettor finns", () => {
      expect(component.summeraEttor([1, 1, 2, 1, 5])).toEqual(3);
    });
  });

  describe("som visar ettor", () => {
    it("borde vara falsk när tärningar är tom array", () => {
      expect(component.farSparaEttor([])).toBeFalse();
    });

    it("borde vara sant när tärningar ej är tom array", () => {
      expect(component.farSparaEttor([1])).toBeTruthy();
    });

    it("borde vara falsk när tärningar finns och spelare har sparade ettor", () => {
      component.spelare.ettor = 1;
      expect(component.farSparaEttor([1])).toBeFalse();
    });
  });

  describe("spara ettor", () => {
    it("borde spara summerade ettor för spelaren", () => {
      component.sparaEttor([1, 1]);
      expect(component.spelare.ettor).toEqual(2);
    });

    it("borde avsluta speldrag", () => {
      spyOn(component.avslutatSpeldrag, 'emit');
      component.sparaEttor([1, 1]);
      expect(component.avslutatSpeldrag.emit).toHaveBeenCalledWith(1);
    });
  });

  describe("summera tvåor", () => {
    it("borde returnera 0 när endast nollor finns", () => {
      expect(component.summeraTvaor([0, 0, 0, 0, 0])).toEqual(0);
    });

    it("borde returnera 10 när fem tvåor finns", () => {
      expect(component.summeraTvaor([2, 2, 2, 2, 2])).toEqual(10);
    });

    it("borde returnera 6 när tre tvåor finns", () => {
      expect(component.summeraTvaor([2, 2, 3, 2, 5])).toEqual(6);
    });
  });

  describe("som visar tvåor", () => {
    it("borde vara falsk när tärningar är tom array", () => {
      expect(component.farSparaTvaor([])).toBeFalse();
    });

    it("borde vara sant när tärningar ej är tom array", () => {
      expect(component.farSparaTvaor([1])).toBeTruthy();
    });

    it("borde vara falsk när tärningar finns och spelare har sparade tvåor", () => {
      component.spelare.tvaor = 2;
      expect(component.farSparaTvaor([1])).toBeFalse();
    });
  });

  describe("spara tvåor", () => {
    it("borde spara summerade tvåor för spelaren", () => {
      component.sparaTvaor([2, 2]);
      expect(component.spelare.tvaor).toEqual(4);
    });

    it("borde avsluta speldrag", () => {
      spyOn(component.avslutatSpeldrag, 'emit');
      component.sparaTvaor([2, 2]);
      expect(component.avslutatSpeldrag.emit).toHaveBeenCalledWith(1);
    });
  });

  describe("summera 'ett par'", () => {
    it("borde returnera 0 när endast nollor finns", () => {
      expect(component.summeraEttPar([0, 0, 0, 0, 0])).toEqual(0);
    });

    it("borde returnera 12 när två sexor finns", () => {
      expect(component.summeraEttPar([6, 6, 2, 2, 2])).toEqual(12);
    });

    it("borde returnera 12 när tre sexor finns", () => {
      expect(component.summeraEttPar([6, 6, 6, 2, 2])).toEqual(12);
    });

    it("borde returnera 0 när en sexa finns", () => {
      expect(component.summeraEttPar([6, 2, 3, 1, 5])).toEqual(0);
    });

    it("borde returnera 10 när två femmor finns", () => {
      expect(component.summeraEttPar([4, 1, 5, 5, 2])).toEqual(10);
    });

    it("borde returnera 8 när två fyror finns", () => {
      expect(component.summeraEttPar([4, 3, 4, 2, 2])).toEqual(8);
    });

    it("borde returnera 6 när två treor finns", () => {
      expect(component.summeraEttPar([5, 3, 3, 2, 2])).toEqual(6);
    });

    it("borde returnera 4 när två tvåor finns", () => {
      expect(component.summeraEttPar([2, 2, 6, 1, 4])).toEqual(4);
    });

    it("borde returnera 2 när två ettor finns", () => {
      expect(component.summeraEttPar([2, 1, 6, 1, 4])).toEqual(2);
    });

    it("borde returnera 10 när högsta paret bland flera par är två femmor", () => {
      expect(component.summeraEttPar([5, 2, 2, 5, 4])).toEqual(10);
    });
  });
});
