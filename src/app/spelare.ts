export class Spelare {

  constructor(public namn: string, public id: number) {
  }

  ettor: any = null;
  tvaor: any = null;
  treor: any = null;
  fyror: any = null;
  femmor: any = null;
  sexor: any = null;
  ettPar: any = null;
  tvaPar: any = null;
  tretal: any = null;
  fyrtal: any = null;
  litenStege: any = null;
  storStege: any = null;
  kak: any = null;
  chans: any = null;
  yatzy: any = null;
  totalt: any = null;

  public summa(): number {
    return this.ettor + this.tvaor + this.treor + this.fyror + this.femmor + this.sexor;
  }

  public bonus(): number {
    return this.summa() >= 63 ? 50 : 0;
  }

  public total(): number {
    return this.summa() + this.bonus() + this.ettPar + this.tvaPar + this.tretal +
      this.fyrtal + this.litenStege + this.storStege + this.kak + this.chans + this.yatzy;
  }
}
