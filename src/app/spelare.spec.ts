import {Spelare} from "./spelare";

describe('Spelare', () => {
  let spelare: Spelare;

  beforeEach(() => {
    spelare = new Spelare("Alice", 1);
  });

  describe("Summera 6 översta villkoren", () => {
    it("borde summera till 3 när ettor är 3", () => {
      spelare.ettor = 3;
      expect(spelare.summa()).toBe(3);
    });

    it("borde summera till 1 när ettor är 1", () => {
      spelare.ettor = 1;
      expect(spelare.summa()).toBe(1);
    });

    it("borde summera till 4 när tvåor är 4", () => {
      spelare.tvaor = 4;
      expect(spelare.summa()).toBe(4);
    });

    it("borde summera till 0 när inga villkor angetts", () => {
      expect(spelare.summa()).toBe(0);
    });

    it("border summera ettor och tvåor till 5", () => {
      spelare.ettor = 1;
      spelare.tvaor = 4;

      expect(spelare.summa()).toBe(5);
    })

    it("borde summera till 6 när treor är 6", () => {
      spelare.treor = 6;
      expect(spelare.summa()).toBe(6);
    });

    it("borde summera till 8 när fyror är 8", () => {
      spelare.fyror = 8;
      expect(spelare.summa()).toBe(8);
    });

    it("borde summera till 5 när femmor är 5", () => {
      spelare.femmor = 5;
      expect(spelare.summa()).toBe(5);
    });

    it("borde summera till 36 när sexor är 36", () => {
      spelare.sexor = 36;
      expect(spelare.summa()).toBe(36);
    });

    it("borde summera till 0 när 'ett par' är 10", () => {
      spelare.ettPar = 10;
      expect(spelare.summa()).toBe(0);
    });

    it("borde summera till 0 när 'två par' är 14", () => {
      spelare.tvaPar = 14;
      expect(spelare.summa()).toBe(0);
    });

    it("borde summera till 0 när tretal är 15", () => {
      spelare.tretal = 15;
      expect(spelare.summa()).toBe(0);
    });

    it("borde summera till 0 när fyrtal är 4", () => {
      spelare.fyrtal = 4;
      expect(spelare.summa()).toBe(0);
    });

    it("borde summera till 0 när 'liten stege' är 15", () => {
      spelare.litenStege = 15;
      expect(spelare.summa()).toBe(0);
    });

    it("borde summera till 0 när 'stor stege' är 20", () => {
      spelare.storStege = 20;
      expect(spelare.summa()).toBe(0);
    });

    it("borde summera till 0 när kåk är 25", () => {
      spelare.kak = 25;
      expect(spelare.summa()).toBe(0);
    });

    it("borde summera till 0 när chans är 18", () => {
      spelare.chans = 18;
      expect(spelare.summa()).toBe(0);
    });

    it("borde summera till 0 när yatzy är 50", () => {
      spelare.yatzy = 50;
      expect(spelare.summa()).toBe(0);
    });
  });

  describe("Beräkna bonus", () => {
    it("borde registrera 50 som bonus när summa för de sex översta villkoren är 63", () => {
      spelare.ettor = 3;
      spelare.tvaor = 6;
      spelare.treor = 9;
      spelare.fyror = 12;
      spelare.femmor = 15;
      spelare.sexor = 18;

      expect(spelare.bonus()).toBe(50);
    });

    it("borde registrera 0 som bonus när summa för de sex översta villkoren är 62", () => {
      spelare.ettor = 2;
      spelare.tvaor = 6;
      spelare.treor = 9;
      spelare.fyror = 12;
      spelare.femmor = 15;
      spelare.sexor = 18;

      expect(spelare.bonus()).toBe(0);
    });
  });

  describe("Summera totalsumma", () => {
    it("borde summera till 1 när ettor är 1", () => {
      spelare.ettor = 1;

      expect(spelare.total()).toBe(1);
    });

    it("borde summera till 10 när 'ett par' är 4 och 'två par' är 6", () => {
      spelare.ettPar = 4;
      spelare.tvaPar = 6;

      expect(spelare.total()).toBe(10);
    });

    it("borde summera till 15 när tretal är 15", () => {
      spelare.tretal = 15;

      expect(spelare.total()).toBe(15);
    });

    it("borde summera till 16 när fyrtal är 16", () => {
      spelare.fyrtal = 16;

      expect(spelare.total()).toBe(16);
    });

    it("borde summera till 15 när 'liten stege' är 15", () => {
      spelare.litenStege = 15;

      expect(spelare.total()).toBe(15);
    });

    it("borde summera till 20 när 'stor stege' är 20", () => {
      spelare.storStege = 20;

      expect(spelare.total()).toBe(20);
    });

    it("borde summera till 25 när kåk är 25", () => {
      spelare.kak = 25;

      expect(spelare.total()).toBe(25);
    });

    it("borde summera till 25 när chans är 25", () => {
      spelare.chans = 25;

      expect(spelare.total()).toBe(25);
    });

    it("borde summera till 50 när yatzy är 50", () => {
      spelare.yatzy = 50;

      expect(spelare.total()).toBe(50);
    });

    it("borde inkludera bonus i totalsumma", () => {
      spelare.ettor = 3;
      spelare.tvaor = 6;
      spelare.treor = 9;
      spelare.fyror = 12;
      spelare.femmor = 15;
      spelare.sexor = 18;

      expect(spelare.total()).toBe(63 + 50);
    });
  });
});
