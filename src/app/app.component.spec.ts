import {TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';
import {PoangComponent} from "./poang/poang.component";
import {MockComponents} from "ng-mocks";

describe('AppComponent', () => {
  let component: AppComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      declarations: [
        AppComponent,
        MockComponents(PoangComponent)
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    const fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });

  it("borde skapa app", () => {
    expect(component).toBeTruthy();
  });

  it("borde initiera tärningarna till 0", () => {
    expect(component.tarningar).toEqual([0, 0, 0, 0, 0]);
  });

  [0, 1, 2, 3, 4].forEach((tärning) => describe(`borde öka tärning ${tärning}`, () => {
    it("till 1", () => {
      växlaTärningsSidor(tärning, 1);
      expect(component.tarningar[tärning]).toEqual(1);
    });

    it("till 2", () => {
      växlaTärningsSidor(tärning, 2);
      expect(component.tarningar[tärning]).toEqual(2);
    });

    it("till 3", () => {
      växlaTärningsSidor(tärning, 3);
      expect(component.tarningar[tärning]).toEqual(3);
    });

    it("till 4", () => {
      växlaTärningsSidor(tärning, 4);
      expect(component.tarningar[tärning]).toEqual(4);
    });

    it("till 5", () => {
      växlaTärningsSidor(tärning, 5);
      expect(component.tarningar[tärning]).toEqual(5);
    });

    it("till 6", () => {
      växlaTärningsSidor(tärning, 6);
      expect(component.tarningar[tärning]).toEqual(6);
    });

    it("till 1", () => {
      växlaTärningsSidor(tärning, 7);
      expect(component.tarningar[tärning]).toEqual(1);
    });
  }));

  function växlaTärningsSidor(tärning: number, sidor: number) {
    for (let i = 0; i < sidor; i++) {
      component.nastaTarningssida(tärning);
    }
  }
});
