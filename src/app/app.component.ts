import {Component} from '@angular/core';
import {Spelare} from "./spelare";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  valtAlternativ: number = 0;
  spelare: Spelare[] = [new Spelare("Spelare 1", 1), new Spelare("Spelare 2", 2), new Spelare("Spelare 3", 3), new Spelare("Spelare 4", 4), new Spelare("Spelare 5", 5)];
  spelareAlternativ = [
    [this.spelare[0], this.spelare[1]],
    [this.spelare[0], this.spelare[1], this.spelare[2]],
    [this.spelare[0], this.spelare[1], this.spelare[2], this.spelare[3]],
    [this.spelare[0], this.spelare[1], this.spelare[2], this.spelare[3], this.spelare[4]],
  ];
  aktuellSpelare = this.spelare[0].id;

  public tarningar = [0, 0, 0, 0, 0];
  public tarningsogon = ["&#9746;", "&#9856;", "&#9857;", "&#9858;", "&#9859;", "&#9860;", "&#9861;"];

  public nastaTarningssida(tärning: number): void {
    this.tarningar[tärning]++;
    if (this.tarningar[tärning] > 6) {
      this.tarningar[tärning] = 1;
    }
  }

  nastaSpelaresSpeldrag(föregåendeSpelaresId: number) {
    // TODO: Ta bort kommentar när metod har enhetstester
    this.tarningar.fill(0);
    let index = this.spelare.findIndex(s => s.id === föregåendeSpelaresId) + 1;
    let antalSpelare = this.spelareAlternativ[this.valtAlternativ].length;
    this.aktuellSpelare = this.spelare[index % antalSpelare].id;
  }
}
